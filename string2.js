// let s = "111.139.161.143";

function printValidIP(s)
{
    const regexExp = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;
    let result = regexExp.test(s);

    if(!result)
    {
        const arr = [];
        // console.log(arr);
        return arr;
    }

  let arr = s.split(".");
let final = [];

for(let i =0; i < arr.length; i++)
{
    let val = parseInt(arr[i]);
    final.push(val);
}

// console.log(final);
return final
    
}

// printValidIP(s);
module.exports = printValidIP;

