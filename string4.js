function titleCase(stringArr) {
    let strAnswer = '';
    // extract the  each string from array
    for(let i =0; i < stringArr.length; i++)
    {
        let str = stringArr[i];
        str = str.toLowerCase();
        // console.log(str);
         str = str.charAt(0).toUpperCase() + str.slice(1);
        //str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
        strAnswer = strAnswer.concat(str, ' ');
        // console.log(strAnswer);


    }
    // console.log(strAnswer);
    return strAnswer;
}
// titleCase(stringArr);

module.exports = titleCase;
